package com.example.lab7;

import javafx.application.Platform;
import javafx.scene.control.TableView;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.concurrent.Semaphore;

class Consumer implements Runnable {
    private Factory factory;
    private int produceTime;
    private Semaphore semaphore = new Semaphore(1);
    private TableView<ThreadInfo> threadInfoTable;
    public Consumer(Factory factory, TableView<ThreadInfo> threadInfoTable, int consumeTime) {
        this.factory = factory;
        this.threadInfoTable = threadInfoTable;
        this.produceTime = consumeTime;
    }

    @Override
    public void run() {
        try {
            while (factory.getTotalResources() > 0 || factory.getTotalGoods() > 0) {
                semaphore.acquire();

                try {
                    while (factory.getTotalGoods() == 0) {
                        updateThreadInfo(Thread.currentThread().getName(), Thread.currentThread().getState(), Thread.currentThread().getPriority(), LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
                        semaphore.release();
                        synchronized (factory) {
                            factory.wait();
                        }
                        semaphore.acquire();
                    }

                    if (factory.getTotalGoods() > 0) {
                        factory.consumeGoods();
                        updateThreadInfo(Thread.currentThread().getName(), Thread.currentThread().getState(), Thread.currentThread().getPriority(), LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
                        Thread.sleep(produceTime);
                    }
                } finally {
                    semaphore.release();
                }
            }
        } catch (InterruptedException e) {
            updateThreadInfo(Thread.currentThread().getName(), Thread.currentThread().getState(), Thread.currentThread().getPriority(), LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
            Thread.currentThread().interrupt();
        }
    }


    private void updateThreadInfo(String threadName, Thread.State state, int priority, String timeStamp) {
        Platform.runLater(() -> {
            Optional<ThreadInfo> existingRow = threadInfoTable.getItems().stream()
                    .filter(row -> row.getThreadName().equals(threadName))
                    .findFirst();

            if (existingRow.isPresent()) {
                ThreadInfo threadInfo = existingRow.get();
                threadInfo.setThreadState(state);
                threadInfo.setThreadPriority(priority);
                threadInfo.setTimeStamp(timeStamp);
                threadInfoTable.refresh();
            } else {
                threadInfoTable.getItems().add(new ThreadInfo(threadName, state, priority, timeStamp));
            }
        });
    }
}
