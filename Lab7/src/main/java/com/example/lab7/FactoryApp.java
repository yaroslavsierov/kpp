package com.example.lab7;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.Random;

public class FactoryApp extends Application {
    private TextArea textArea = new TextArea();
    private Thread[] consumers;
    private Thread[] producers;
    private TextField consumerCountField = new TextField("");
    private TextField producerCountField = new TextField("");
    private TableView<ThreadInfo> threadInfoTable = new TableView<>();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Factory Simulation");

        Label producersLabel = new Label("Number of Producers:");
        consumerCountField.setPrefWidth(50);

        Label consumersLabel = new Label("Number of Consumers:");
        producerCountField.setPrefWidth(50);

        Button startButton = new Button("Create Threads");
        startButton.setOnAction(e -> startAllThreads());
        Button stopButton = new Button("Terminate Threads");
        stopButton.setOnAction(e -> stopAllThreads());

        TableColumn<ThreadInfo, String> nameColumn = new TableColumn<>("Thread Name");
        nameColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getThreadName()));

        TableColumn<ThreadInfo, Thread.State> stateColumn = new TableColumn<>("Thread State");
        stateColumn.setCellValueFactory(data -> new SimpleObjectProperty<>(data.getValue().getThreadState()));

        TableColumn<ThreadInfo, Integer> priorityColumn = new TableColumn<>("Thread Priority");
        priorityColumn.setCellValueFactory(data -> new SimpleIntegerProperty(data.getValue().getThreadPriority()).asObject());

        TableColumn<ThreadInfo, String> timeColumn = new TableColumn<>("Time Stamp");
        timeColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getTimeStamp()));

        nameColumn.setMinWidth(150);
        stateColumn.setMinWidth(150);
        priorityColumn.setMinWidth(150);
        timeColumn.setMinWidth(150);

        threadInfoTable.getColumns().addAll(nameColumn, stateColumn, priorityColumn, timeColumn);

        textArea.setPrefHeight(400);
        HBox inputFieldsBox = new HBox(10, producersLabel, producerCountField, consumersLabel, consumerCountField);
        HBox buttonsBox = new HBox(10, startButton, stopButton);
        VBox vbox = new VBox(inputFieldsBox, buttonsBox, textArea, threadInfoTable);
        Scene scene = new Scene(vbox, 500, 500);
        primaryStage.setWidth(600);
        primaryStage.setHeight(800);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void updateThreadInfo(String threadName, Thread.State state, int priority, String timeStamp) {
        Platform.runLater(() -> {
            Optional<ThreadInfo> existingRow = threadInfoTable.getItems().stream()
                    .filter(row -> row.getThreadName().equals(threadName))
                    .findFirst();

            if (existingRow.isPresent()) {
                ThreadInfo threadInfo = existingRow.get();
                threadInfo.setThreadState(state);
                threadInfo.setThreadPriority(priority);
                threadInfo.setTimeStamp(timeStamp);
                threadInfoTable.refresh();
            } else {
                threadInfoTable.getItems().add(new ThreadInfo(threadName, state, priority, timeStamp));
            }
        });
    }


    private void startAllThreads() {
        int consumerCount = Integer.parseInt(consumerCountField.getText());
        int producerCount = Integer.parseInt(producerCountField.getText());
        int time = 1500;

        Factory factory = new Factory(100, textArea);
        consumers = new Thread[consumerCount];
        Random random = new Random();
        for (int i = 0; i < consumers.length; i++) {
            consumers[i] = new Thread(new Consumer(factory, threadInfoTable, time), "Consumer " + (i + 1));
            consumers[i].setPriority(random.nextInt(10) + 1);
            consumers[i].start();
        }

        producers = new Thread[producerCount];
        random = new Random();
        for (int i = 0; i < producers.length; i++) {
            producers[i] = new Thread(new Producer(factory, threadInfoTable, time), "Producer " + (i + 1));
            producers[i].setPriority(random.nextInt(10) + 1);
            producers[i].start();
        }
    }

    private void stopAllThreads() {
        for (Thread consumer : consumers) {
            consumer.interrupt();
            updateThreadInfo(consumer.getName(), consumer.getState(), consumer.getPriority(), LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
        }

        for (Thread producer : producers) {
            producer.interrupt();
            updateThreadInfo(producer.getName(), producer.getState(), producer.getPriority(), LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
        }
    }

}
