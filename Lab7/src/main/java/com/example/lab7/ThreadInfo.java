package com.example.lab7;

public class ThreadInfo {
    private String threadName;
    private Thread.State threadState;
    private int threadPriority;
    private String timeStamp;

    public ThreadInfo(String threadName, Thread.State threadState, int threadPriority, String timeStamp) {
        this.threadName = threadName;
        this.threadState = threadState;
        this.threadPriority = threadPriority;
        this.timeStamp = timeStamp;
    }

    public String getThreadName() {
        return threadName;
    }

    public Thread.State getThreadState() {
        return threadState;
    }

    public int getThreadPriority() {
        return threadPriority;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setThreadState(Thread.State threadState) {
        this.threadState = threadState;
    }

    public void setThreadPriority(int threadPriority) {
        this.threadPriority = threadPriority;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}

