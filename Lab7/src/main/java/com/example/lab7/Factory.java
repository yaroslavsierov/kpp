package com.example.lab7;


import javafx.scene.control.TextArea;

import java.util.concurrent.Semaphore;

class Factory {
    private int totalResources;
    private int totalGoods;
    private TextArea textArea;
    private Semaphore semaphore;

    public Factory(int totalResources, TextArea textArea) {
        this.totalResources = totalResources;
        this.semaphore = new Semaphore(totalResources);
        this.textArea = textArea;
        this.totalGoods = 0;
    }

    public void useResources() throws InterruptedException {
        semaphore.acquire();
        synchronized(this) {
            totalResources--;
            updateUI(Thread.currentThread().getName() + " has taken a resource. Total resource left: " + totalResources);
        }
        semaphore.release();
    }

    public void produceGoods() throws InterruptedException {
        semaphore.acquire();
        synchronized(this) {
            totalGoods++;
            updateUI(Thread.currentThread().getName() + " has produced a product. There are " + totalGoods + " products in total ");
        }
        semaphore.release();
    }

    public void consumeGoods() throws InterruptedException {
        semaphore.acquire();
        synchronized(this) {
            if (totalGoods > 0) {
                totalGoods--;
                updateUI(Thread.currentThread().getName() + " has bought a product. Total products left: " + totalGoods);
            }
        }
        semaphore.release();
    }


    private void updateUI(String message) {
        javafx.application.Platform.runLater(() -> textArea.appendText(message + "\n"));
    }

    public int getTotalResources()
    {
        return totalResources;
    }
    public int getTotalGoods()
    {
        return totalGoods;
    }
}
