package com.example.lab7;
import javafx.application.Platform;
import javafx.scene.control.TableView;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.concurrent.Semaphore;

class Producer implements Runnable {
    private Factory factory;
    private TableView<ThreadInfo> threadInfoTable;
    private int produceTime;
    private Semaphore semaphore = new Semaphore(1);

    public Producer(Factory factory, TableView<ThreadInfo> threadInfoTable, int produceTime) {
        this.factory = factory;
        this.threadInfoTable = threadInfoTable;
        this.produceTime = produceTime;
    }

    @Override
    public void run() {
        try {
            while (factory.getTotalResources() > 0) {
                semaphore.acquire();
                factory.useResources();
                try {
                    updateThreadInfo(Thread.currentThread().getName(), Thread.currentThread().getState(), Thread.currentThread().getPriority(), LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
                    Thread.sleep(produceTime);
                    factory.produceGoods();

                    synchronized (factory) {
                        factory.notify();
                    }
                } finally {
                    semaphore.release();
                }
            }
        } catch (InterruptedException e) {
            updateThreadInfo(Thread.currentThread().getName(), Thread.currentThread().getState(), Thread.currentThread().getPriority(), LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
            Thread.currentThread().interrupt();
        }
    }


    private void updateThreadInfo(String threadName, Thread.State state, int priority, String timeStamp) {
        Platform.runLater(() -> {
            Optional<ThreadInfo> existingRow = threadInfoTable.getItems().stream()
                    .filter(row -> row.getThreadName().equals(threadName))
                    .findFirst();

            if (existingRow.isPresent()) {
                ThreadInfo threadInfo = existingRow.get();
                threadInfo.setThreadState(state);
                threadInfo.setThreadPriority(priority);
                threadInfo.setTimeStamp(timeStamp);
                threadInfoTable.refresh();
            } else {
                threadInfoTable.getItems().add(new ThreadInfo(threadName, state, priority, timeStamp));
            }
        });
    }
}

