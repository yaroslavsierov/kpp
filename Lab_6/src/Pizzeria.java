import java.io.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Pizzeria implements Serializable {
    private List<Pizza> pizzas;
    private List<Customer> customers;
    private Map<Pizza, List<Customer>> pizzaCustomers;

    public Pizzeria() {
        pizzas = new ArrayList<>();
        customers = new ArrayList<>();
    }

    public Pizzeria(List<Pizza> pizzas, List<Customer> customers) {
        this.pizzas = pizzas;
        this.customers = customers;
    }

    public void readPizzasFromFile(String filename) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] pizzaData = line.split(",");
                if (pizzaData.length == 4) {
                    String name = pizzaData[0].trim();
                    double weight = Double.parseDouble(pizzaData[1].trim());
                    double cost = Double.parseDouble(pizzaData[2].trim());
                    String[] ingredientsArray = pizzaData[3].trim().split(";");
                    List<String> ingredients = new ArrayList<>(List.of(ingredientsArray));

                    Pizza pizza = new Pizza(name, weight, cost, ingredients);
                    pizzas.add(pizza);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readCustomersFromFile(String filePath) {
        try (Scanner scanner = new Scanner(new File(filePath))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                Customer customer = parseCustomer(line);
                customers.add(customer);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Customer parseCustomer(String line) {
        String[] parts = line.split(",");
        int number = Integer.parseInt(parts[0].trim());
        String address = parts[1].trim();
        List<Order> orders = new ArrayList<>();

        for (int i = 2; i < parts.length; i += 3) {
            String pizzaName = parts[i].trim();
            LocalDateTime orderTime = LocalDateTime.parse("2023-01-01T" + parts[i + 1].trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));
            LocalDateTime deliveryTime = LocalDateTime.parse("2023-01-01T" + parts[i + 2].trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));

            Pizza pizza = getPizzaByName(pizzaName);

            Order order = new Order(pizza, orderTime, deliveryTime);
            orders.add(order);
        }
        return new Customer(number, address, orders);
    }
    public Pizza getPizzaByName(String name) {
        return pizzas.stream()
                .filter(pizza -> pizza.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    public List<Customer> sortOrdersByDeliveryTime() {
        return customers.stream()
                .sorted(Comparator.comparing(customer -> customer.getOrders().get(0).getDeliveryTime()))
                .collect(Collectors.toList());
    }

    public List<String> getAddressesForCustomersWithMoreThanTwoPizzas() {
        return customers.stream()
                .filter(customer -> customer.getOrders().size() > 2)
                .map(Customer::getAddress)
                .collect(Collectors.toList());
    }

    public long countUsersByPizzaName(String pizzaName) {
        return customers.stream()
                .filter(customer -> customer.getOrdersNames().contains(pizzaName))
                .count();
    }

    public Customer getCustomerWithMostOrders() {
        return customers.stream()
                .max(Comparator.comparingInt(customer -> customer.getOrders().size()))
                .orElse(null);
    }

    public Map<Pizza, List<Customer>> getPizzaCustomers() {
        associatePizzasWithCustomers();
        return pizzaCustomers;
    }

    public List<Order> rescheduleOrders() {
        return customers.stream()
                .flatMap(customer -> customer.getOrders().stream()
                        .filter(order -> isTimeDifferenceGreaterThan(order.getOrderTime(), order.getDeliveryTime()))
                        .map(order -> new AbstractMap.SimpleEntry<>(customer, order)))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    private boolean isTimeDifferenceGreaterThan(LocalDateTime orderTime, LocalDateTime deliveryTime) {
        Duration timeDifference = Duration.between(orderTime, deliveryTime);
        return timeDifference.toMinutes() > 30;
    }
    public Duration calculateDelay(LocalDateTime orderTime, LocalDateTime deliveryTime) {
        return Duration.between(orderTime, deliveryTime).minusMinutes(30);
    }



    public void associatePizzasWithCustomers() {
        pizzaCustomers = customers.stream()
                .flatMap(customer -> customer.getOrders().stream().map(order -> new AbstractMap.SimpleEntry<>(order.getPizza(), customer)))
                .collect(Collectors.groupingBy(
                        Map.Entry::getKey,
                        Collectors.mapping(Map.Entry::getValue, Collectors.toList())
                ));
    }





    public void printCustomers(List<Customer> customerList) {
        customerList.forEach(System.out::println);
    }

    public List<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(List<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public void addPizza(Pizza pizza) {
        pizzas.add(pizza);
    }

    public void addCustomer(Customer customer) {
        customers.add(customer);
    }

    public void serializeCollections() {
        try (ObjectOutputStream pizzaStream = new ObjectOutputStream(new FileOutputStream("pizzas.ser"));
             ObjectOutputStream customerStream = new ObjectOutputStream(new FileOutputStream("customers.ser"))) {
            pizzaStream.writeObject(pizzas);
            customerStream.writeObject(customers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deserializeCollections() {
        try (ObjectInputStream pizzaStream = new ObjectInputStream(new FileInputStream("pizzas.ser"));
             ObjectInputStream customerStream = new ObjectInputStream(new FileInputStream("customers.ser"))) {
            pizzas = (List<Pizza>) pizzaStream.readObject();
            customers = (List<Customer>) customerStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}