import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Pizzeria pizzeria = new Pizzeria();

        pizzeria.readPizzasFromFile("pizzas.txt");
        pizzeria.readCustomersFromFile("customers.txt");

        pizzeria.serializeCollections();

        List<Customer> allCustomers = pizzeria.getCustomers();
        System.out.println("List of all customers:");
        pizzeria.printCustomers(allCustomers);

        Scanner scanner = new Scanner(System.in);
        int choice;

        do {
            System.out.println("1. Відсортувати всі замовлення за часом доставки");
            System.out.println("2. Створити список адрес для користувачів, що замовили більше ніж 2 піцци");
            System.out.println("3. Перевірити, скільки користувачів замовили піццу з заданою назвою");
            System.out.println("4. Знайти найбільшу кількість піц, замовлених користувачем");
            System.out.println("5. Створити колекцію з переліком піц та списками їх замовників");
            System.out.println("6. Створити список не виконаних замовлень та вказати час перетермінування");
            System.out.println("0. Вийти");

            System.out.print("Оберіть дію: ");
            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    List<Customer> sortedCustomers = pizzeria.sortOrdersByDeliveryTime();
                    pizzeria.printCustomers(sortedCustomers);
                    break;
                case 2:
                    List<String> addresses = pizzeria.getAddressesForCustomersWithMoreThanTwoPizzas();
                    System.out.println("Адреси користувачів, що замовили більше ніж 2 піцци: " + addresses);
                    break;
                case 3:
                    System.out.print("Введіть назву піци: ");
                    scanner.nextLine();
                    String pizzaName = scanner.nextLine();
                    long count = pizzeria.countUsersByPizzaName(pizzaName);
                    System.out.println("Кількість користувачів, що замовили піцу " + pizzaName + ": " + count);
                    break;
                case 4:
                    Customer maxPizzaCustomer = pizzeria.getCustomerWithMostOrders();
                    System.out.println("Користувач з найбільшою кількістю замовлених піц: " + maxPizzaCustomer);
                    break;
                case 5:
                    Map<Pizza, List<Customer>> pizzaCustomers = pizzeria.getPizzaCustomers();
                    pizzaCustomers.forEach((pizza, customers) -> {
                        System.out.println("Піца: " + pizza.getName());
                        System.out.println("Замовники: " + customers);
                        System.out.println("-----");
                    });
                    break;
                case 6:
                    List<Order> delayedOrders = pizzeria.rescheduleOrders();

                    System.out.println("Замовлення з перетермінуванням:");
                    delayedOrders.forEach(order -> {
                        Duration delay = pizzeria.calculateDelay(order.getOrderTime(), order.getDeliveryTime());
                        System.out.println(order.getPizza().getName() +
                                ", Order Time: " + order.getOrderTime() +
                                ", Delivery Time: " + order.getDeliveryTime() +
                                ", Delay: " + delay.toHoursPart() + " hours and " + delay.toMinutesPart() + " minutes");
                    });
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Неправильний вибір");
                    break;
            }
        } while (choice != 0);
        pizzeria.deserializeCollections();
    }
}

