import java.io.Serializable;
import java.time.LocalDateTime;

public class Order implements Serializable {
    private Pizza pizza;
    private LocalDateTime orderTime;
    private LocalDateTime deliveryTime;

    public Order(Pizza pizza, LocalDateTime orderTime, LocalDateTime deliveryTime) {
        this.pizza = pizza;
        this.orderTime = orderTime;
        this.deliveryTime = deliveryTime;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public LocalDateTime getOrderTime() {
        return orderTime;
    }

    public LocalDateTime getDeliveryTime() {
        return deliveryTime;
    }
}