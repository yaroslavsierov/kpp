import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Customer implements Serializable {
    private int number;
    private String address;
    private List<Order> orders;

    public Customer(int number, String address, List<Order> orders) {
        this.number = number;
        this.address = address;
        this.orders = orders;
    }

    public int getNumber() {
        return number;
    }

    public String getAddress() {
        return address;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }


    public ArrayList<String> getOrdersNames() {
        ArrayList<String> names = new ArrayList<>();
        for (Order order : orders) {
            names.add(order.getPizza().getName());
        }
        return names;
    }


    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Customer{№").append(number)
                .append(", address='").append(address).append('\'')
                .append(", orders=[");

        for (Order order : orders) {
            result.append("(")
                    .append(order.getPizza().getName()).append(", ")
                    .append("Order Time: ").append(order.getOrderTime()).append(", ")
                    .append("Delivery Time: ").append(order.getDeliveryTime())
                    .append("), ");
        }

        if (!orders.isEmpty()) {
            result.delete(result.length() - 2, result.length());
        }

        result.append("]}");

        return result.toString();
    }
}